#!/usr/bin/python

#SYN:xmencn00

from optparse import OptionParser
import sys
import re

debug = 0 #debug flag, debug prints...

inf = None #input file descriptor
outf = None #output file descriptor
formatf = None #format file descriptor
br = False #tag <br /> before \n


#prints debug messages to stderr if debug flag is set, else do nothing
def print_debug(message):
	if debug == 1:
		print("DEBUG: " + message, file=sys.stderr)


#print err specific message and exit program with code err
def exit_error(err):
	if err == 1:
		print("Wrong arguments", file=sys.stderr)
	elif err == 2:
		print("Error opening input file", file=sys.stderr)
	elif err == 3:
		print("Error opening output file", file=sys.stderr)
	elif err == 4:
		print("Error format file content", file=sys.stderr)
	else:
		print("Unrecognized error", file=sys.stderr)

	sys.exit(err)


#Prints help message to user
def print_help():
	print("-f --format=file == Formatovaci soubor s reg. vyrazy a prikazy\n"+
	      "-i --input=file  == Soubor se vstupnim textem\n"+
	      "-o --output=file == Soubor pro vystup\n"+
	      "-b --br          == Prida tag <br /> na konce radku\n"+
	      "-h --help        == Vypis napovedy")


#process params, open files for input, output, format
def params():
	parser = OptionParser(add_help_option=False)
	parser.add_option("-f", "--format", dest="format_file", help="Formatovaci soubor s reg. vyrazy a prikazy")
	parser.add_option("-i", "--input", dest="input_file", help="Soubor se vstupnim textem")
	parser.add_option("-o", "--output", dest="output_file", help="Soubor pro vystup")
	parser.add_option("-b", "--br", action="store_true", dest="br", default=False, help="Prida tag <br /> na konce radku")
	parser.add_option("-h", "--help", action="store_true", dest="help", default=False, help="Vypis napovedy")

	try:
		(options, args) = parser.parse_args()
	except SystemExit:
		exit_error(1)

	if options.help == True:
		print_help()
		exit(0)

	global inf, outf, formatf, br

	if options.input_file is not None: #--input arg set
		print_debug("Input file: " + options.input_file)
		try:
			inf = open(options.input_file, 'r', encoding='utf-8')
		except:
			exit_error(2)
	else:
		print_debug("Input file: stdin")
		inf = sys.stdin #notset --input

	if options.output_file is not None: #--output arg set
		print_debug("Output file: " + options.output_file)
		try:
			outf = open(options.output_file, 'w', encoding='utf-8')
		except:
			exit_error(3)
	else:
		print_debug("Output file: stdout")
		outf = sys.stdout #notset --output

	if options.format_file is not None: #--format arg set
		print_debug("Format file: " + options.format_file)
		try:
			formatf = open(options.format_file, 'r')
		except:
			exit_error(4)
	else:
		print_debug("Format file: None")
		formatf = None

	br = options.br
	if (br == True):
		print_debug("Br: True")
	else:
		print_debug("Br: False")


#get file with format regexs and commands and parse content, return list for each
def parse_format(format_file):
	if format_file is None:
		return None, None

	r1 = re.compile('^(.*?)\t') #catch regexs
	r2 = re.compile('^.*?\t+(.*)$') #catch commands

	reg_list = []
	com_list = []

	for line in formatf:
		m1 = r1.search(line)
		m2 = r2.search(line)
		if m1 and m2:
			com = m2.group(1)
			com = re.sub('[\t ]', '', com) #cut spaces and tabulators from command list
			com = com.split(',') #split commands to list

			com_list.append(com)
			reg_list.append(m1.group(1))

			print_debug("Format: " + m1.group(1) + "|" + ','.join(com))

	return (reg_list, com_list)

#get one format file regex and translate to python regex
#make really ugly regexs, but in easy way
def regex_compile(rx):
	regex = ''
	state = 0
	neg = ''
	prev_c = ''

	#Rozsireni NQS, spravne vyhodnoceni vicenasobnych kvantifikatoru '+' '*'
	rx = re.sub('[+]+', '+', rx) #multiple '+' series to one '+'
	rx = re.sub('[*+]{2,}', '*', rx) #each 2 or more groups with '*' to '*'

	for c in rx:
		if (ord(c) < 32):
			exit_error(4) #only ascii 32+ are allowed

		if (state == 0):
			if (c == '.'): #concatenation (meaningless)
				if (prev_c == '' or prev_c == '.' or prev_c == '!'): #non sense combination
					print_debug("Regex error: " + prev_c + c)
					exit_error(4)
			elif (c == '!'):
				neg = '^'
			elif (c == '%'):
				state = 1
			elif (c in '|*+()'):
				regex += c
			elif (c in '^[]\\'):
				regex += '[' + neg + '\\' + c + ']'
				neg = ''
			else:
				regex += '[' + neg + c + ']'
				neg = ''
		
		elif (state == 1): #previous == %
			if (c == 'a'):
				regex += '.'
			else:
				regex += '[' + neg

				if (c == 's'):
					regex += ' \t\n\r\f\v'
				elif (c == 'd'):
					regex += '0-9'
				elif (c == 'l'):
					regex += 'a-z'
				elif (c == 'L'):
					regex += 'A-Z'
				elif (c == 'w'):
					regex += 'a-zA-Z'
				elif (c == 'W'):
					regex += 'a-zA-Z0-9'
				elif (c == 't'):
					regex += '\t'
				elif (c == 'n'):
					regex += '\n'
				elif (c in '.|!*+()%'):
					regex += c
				else:
					print_debug("Regex error: %" + c)
					exit_error(4)

				regex += ']'

			state = 0
			neg = ''

		prev_c = c


	print_debug("Regex compile: " + rx + ' => ' + regex) #input => output

	try:
		re.compile(regex)
	except:
		exit_error(4)

	return regex


#Params: input file, list of python regexs and list of coresponding commands
#Return: Generated output string
def put_tags(in_str, reg_list, com_list):
	tags = [''] * (len(in_str) + 1) #to put tags in, join then

	if (reg_list is not None and com_list is not None):
		for regex, commands in zip(reg_list, com_list): #iterate through both lists simultaneously
			print_debug("Apply regex: " + regex)
			html_tags(commands, True) #just checks commands validity

			for match in re.finditer(regex, in_str, re.MULTILINE|re.DOTALL):
				if (match.start() != match.end()): #empty match
					tags[match.start()] += html_tags(commands, True) #Opening tags
					tags[match.end()] = html_tags(commands, False) + tags[match.end()]#Closing tags

	final_str = ''

	for i in range(0, len(in_str)):
		final_str += tags[i] + in_str[i]

	final_str += tags[len(in_str)] #tag at the end of string

	return final_str


#Get list of commands and generate string of HTML tags to be passed into input
#If opening == True, generate opening tags (e.g. <br>, <i>)
#If opening == False, generate closing tags (e.g. <br />, </i>)
def html_tags(commands_list, opening):
	result = ''

	if (opening == True): #Opening tags
		for command in commands_list:
			if (command == 'bold'):
				result += '<b>'
			elif (command == 'italic'):
				result += '<i>'
			elif (command == 'underline'):
				result += '<u>'
			elif (command == 'teletype'):
				result += '<tt>'
			elif (re.match('color:[0-9A-F]{6}', command)):
				result += '<font color=#' + command[6:12] +'>'
			elif (re.match('size:[1-7]', command)):
				result += '<font size=' + command[5:6] + '>'
			else:
				exit_error(4)
			
			print_debug("\t\t" + command)
	else:
		for command in reversed(commands_list): #reverse order of tags!
			if (command == 'bold'):
				result += '</b>'
			elif (command == 'italic'):
				result += '</i>'
			elif (command == 'underline'):
				result += '</u>'
			elif (command == 'teletype'):
				result += '</tt>'
			elif (re.match('color:[0-9A-F]{6}', command)):
				result += '</font>'
			elif (re.match('size:[1-7]', command)):
				result += '</font>'
			else:
				exit_error(4)

			print_debug("\t\t/" + command)

	return result


# C like main function
def main():
	params() #when error, func kills program

	(reg_list, com_list) = parse_format(formatf)

	py_reg_list = []

	if (reg_list is not None): #cant iterate throught None type
		for reg in reg_list:
			py_reg_list.append(regex_compile(reg))

	in_string = inf.read()

	out_string = put_tags(in_string, py_reg_list, com_list)

	if (br == True):
		out_string = re.sub('\n', '<br />\n', out_string) #add <br /> tag before \n

	outf.write(out_string)

	return 0


main() #<<Program start